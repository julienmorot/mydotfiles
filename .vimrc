" Don't try to be vi compatible
set nocompatible

set background=dark
colorscheme murphy

syntax on
set number

" Helps force plugins to load correctly when it is turned back on below
filetype off

" For plugins to load correctly
filetype plugin indent on

" Security
set modelines=0

set smarttab
set smartindent
set autoindent
set shiftwidth=4
set tabstop=4
set cindent
set expandtab

set ignorecase
set smartcase
set incsearch

" Blink cursor on error instead of beeping (grr)
set visualbell

" Encoding
set encoding=utf-8

" Whitespace
set wrap
set textwidth=79
set formatoptions=tcqrn1

" Allow hidden buffers
set hidden

" Rendering
set ttyfast

" Status bar
set laststatus=2

" Last line
set showmode
set showcmd


